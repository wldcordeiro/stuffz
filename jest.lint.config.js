const { resolve } = require('path');
module.exports = {
  rootDir: resolve(__dirname),
  displayName: 'eslint',
  runner: 'jest-runner-eslint',
  testMatch: ['<rootDir>/packages/*/src/**/*.js', '<rootDir>/*.js'],
  testPathIgnorePatterns: ['/node_modules/', '/dist/'],
};
