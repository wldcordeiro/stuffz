module.exports = {
  collectCoverage: true,
  projects: [
    'packages/*/jest.config.js',
    'jest.lint.config.js',
    'jest.prettier.config.js',
  ],
};
