module.exports = {
  collectCoverage: true,
  projects: ['packages/*/jest.config.js'],
};
