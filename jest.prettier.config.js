const { resolve } = require('path');
module.exports = {
  rootDir: resolve(__dirname),
  displayName: 'prettier',
  runner: './packages/jest-runner-prettier/src/index.js',
  testMatch: ['<rootDir>/{docs,packages}/**/*.js', '<rootDir>/*.js'],
  testPathIgnorePatterns: ['/node_modules/', '/dist/'],
  moduleFileExtensions: ['js'],
};
