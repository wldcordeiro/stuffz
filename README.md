# Stuffz

A monorepo for my various libraries and packages I use and share because it's a
pain to do this for every repo.

## Getting started

Run `yarn` to get dependencies installed and linked across packages.

```sh
yarn
```

## Commands

* `yarn build` - Build all packages
* `yarn build --scope <package-blob>` - Build matching packages
* `yarn commit` - Run the commitizen prompt, this makes it easy to match commit format
* `yarn docz:build` - Build the documentation/component playground site
* `yarn format` - Run Prettier across the project
* `yarn lint` - Run ESLint across the project
* `yarn start` - Run the documentation/component playground site
* `yarn test` - Run tests across the project
* `yarn validate` - Run ESLint, Prettier, and tests across the project
