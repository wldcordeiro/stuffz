/* eslint-env jest */
import resultsStates from './results-states';
import { clearResult, withResultId } from './actions';
import { createResult } from './create-result';

const TEST_ACTION = 'TEST_ACTION';
const testAction = () => ({ type: TEST_ACTION });
const {
  resultAction: testActionResult,
  resultSelector: getTestActionResult,
} = createResult(TEST_ACTION);
const re$ultId = 'result-id-123';

describe('results reducer', () => {
  test('default state', () => {
    const state = resultsStates(undefined, { type: 'TEST' });
    expect(state).toEqual({});
  });

  test('suppressed result', () => {
    const {
      resultAction: testActionResult,
      resultSelector: getTestAction,
    } = createResult(TEST_ACTION, { suppress: true });
    let state = resultsStates(undefined, { type: 'TEST' });
    expect(state).toEqual({});
    state = resultsStates(state, testActionResult({ success: {} }));
    expect(state).toEqual({});
    expect(getTestAction(state)).toBeUndefined();
  });

  test('success result', () => {
    let state = resultsStates(undefined, { type: 'TEST' });
    expect(state).toEqual({});
    state = resultsStates(state, testActionResult({ success: {} }));
    expect(state).toEqual({
      [testActionResult]: { latest: true },
    });
    expect(getTestActionResult(state)).toBe(true);
  });
  test('error result', () => {
    let state = resultsStates(undefined, { type: 'TEST' });
    expect(state).toEqual({});
    state = resultsStates(state, testActionResult({ error: {} }));
    expect(state).toEqual({
      [testActionResult]: { latest: false },
    });
    expect(getTestActionResult(state)).toBe(false);
  });

  test('updates latest', () => {
    let state = resultsStates(undefined, { type: 'TEST' });
    expect(state).toEqual({});
    state = resultsStates(state, testActionResult({ success: {} }));
    expect(state).toEqual({
      [testActionResult]: { latest: true },
    });
    expect(getTestActionResult(state)).toBe(true);
    state = resultsStates(state, testActionResult({ error: {} }));
    expect(state).toEqual({
      [testActionResult]: { latest: false },
    });
    expect(getTestActionResult(state)).toBe(false);
    state = resultsStates(state, testActionResult({ success: {} }));
    expect(state).toEqual({
      [testActionResult]: { latest: true },
    });
    expect(getTestActionResult(state)).toBe(true);
  });

  test('clear result', () => {
    let state = resultsStates(undefined, { type: 'TEST' });
    expect(state).toEqual({});
    state = resultsStates(state, testActionResult({ success: {} }));
    expect(state).toEqual({
      [testActionResult]: { latest: true },
    });
    expect(getTestActionResult(state)).toBe(true);
    state = resultsStates(state, clearResult(testActionResult.toString()));
    expect(state).toEqual({
      [testActionResult]: { latest: null },
    });
    expect(getTestActionResult(state)).toBe(null);
  });

  test('updates specific result', () => {
    let state = resultsStates(undefined, { type: 'TEST' });
    expect(state).toEqual({});
    state = resultsStates(state, testActionResult({ success: { re$ultId } }));
    expect(state).toEqual({
      [testActionResult]: { [re$ultId]: true },
    });
    expect(getTestActionResult(state, re$ultId)).toBe(true);
  });

  test('clears when initiating action is sent', () => {
    let state = resultsStates(undefined, { type: 'TEST' });
    expect(state).toEqual({});
    state = resultsStates(state, testActionResult({ success: {} }));
    expect(state).toEqual({
      [testActionResult]: { latest: true },
    });
    expect(getTestActionResult(state)).toBe(true);
    state = resultsStates(state, testAction());
    expect(state).toEqual({
      [testActionResult]: { latest: null },
    });
    expect(getTestActionResult(state)).toBe(null);
  });
});

describe('withResultId', () => {
  test('wraps action and adds re$ultId property', () => {
    expect(testAction()).toEqual({ type: TEST_ACTION });
    expect(withResultId(testAction, re$ultId)()).toEqual({
      type: TEST_ACTION,
      re$ultId,
    });
  });
});
