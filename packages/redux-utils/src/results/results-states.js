import { CLEAR_RESULT } from './actions';
export const typeSuffix = '_RE$ULT';

function latestOrKeyed(key, state, value, { re$ultId }) {
  if (re$ultId == null) {
    return { ...state, [key]: { ...state[key], latest: value } };
  }
  return { ...state, [key]: { ...state[key], [re$ultId]: value } };
}

export default function resultsStates(state = {}, action) {
  const isResultAction = action.type.endsWith(typeSuffix);
  const actionAsResult = `${action.type}${typeSuffix}`;
  if (!isResultAction && state[actionAsResult] != null) {
    return latestOrKeyed(actionAsResult, state, null, action);
  }

  if (isResultAction) {
    if (action.suppress) return state;
    if (action.success) {
      return latestOrKeyed(action.type, state, true, action.success);
    }

    return latestOrKeyed(action.type, state, false, action.error);
  }

  if (action.type === CLEAR_RESULT) {
    return {
      ...state,
      [action.resultKey]: {
        ...state[action.resultKey],
        [action.id]: null,
      },
    };
  }
  return state;
}
