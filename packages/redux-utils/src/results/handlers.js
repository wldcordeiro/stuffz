import { empty } from 'rxjs';

export function devOnlyConsoleHandler({ handlerOpts: { rawErr } }) {
  if (process.env.NODE_ENV !== 'production') {
    /* eslint-disable no-console */
    console.error(rawErr);
    /* eslint-enable no-console */
  }
  return empty();
}
