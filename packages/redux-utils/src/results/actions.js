export const CLEAR_RESULT = 'results/CLEAR_RESULT';
export function clearResult(resultKey, id = 'latest') {
  return { type: CLEAR_RESULT, resultKey, id };
}

export function withResultId(actionCreator, re$ultId) {
  return function(...args) {
    return { ...actionCreator(...args), re$ultId };
  };
}
