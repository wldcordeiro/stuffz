/* eslint-env jest */
import { of } from 'rxjs';
import { snapshotEpic } from '../snapshot-epic';
import { createResult } from './create-result';
import { errorAggregatorEpic, successAggregatorEpic } from './epics';

const FAKE_ACTION = 'TEST_THING';
const HANDLED_AWAY = 'HANDLED_AWAY';
const fakeAction = () => ({ type: FAKE_ACTION });
const fakeOutAction = (action, stream$) => ({
  type: HANDLED_AWAY,
  action,
  stream$,
});
const mockHandler = (...args) => of(fakeOutAction(...args));
const { success: fakeActionSuccess, error: fakeActionError } = createResult(
  FAKE_ACTION,
  {
    errorHandler: mockHandler,
    successHandler: mockHandler,
  }
);
const state$ = of({});

describe('errorAggregatorEpic', () => {
  const errMsg = 'Some Thing Broke Real Bad';
  const errName = 'DoomsdayError';
  const rawErr = new Error(errMsg);
  rawErr.name = errName;
  rawErr.stack = `${errName}: ${errMsg}`;
  const text = 'Something failed.';
  const action$ = fakeActionError({}, { text, action: fakeAction() })(rawErr);
  test('filters non-suppressed error results & uses default handler', done => {
    snapshotEpic(errorAggregatorEpic(action$, state$), done);
  });
});

describe('successAggregatorEpic', () => {
  test('filters non-suppressed success results & uses provided handler', done => {
    const action$ = of(
      fakeActionSuccess({}, { text: 'Woo cool tapes created!' })
    );
    snapshotEpic(successAggregatorEpic(action$, state$), done);
  });
});
