/* eslint-env jest */
import { typeSuffix } from './results-states';
import { CLEAR_RESULT } from './actions';
import { createResult, defaultOptions } from './create-result';

describe('Redux utilities', () => {
  describe('createResult', () => {
    test('returns result action creator and action type', () => {
      const action = 'SOME_ACTION';
      const { resultAction, success, error, clear, type } = createResult(
        action
      );
      expect(type).toEqual(`${action}${typeSuffix}`);
      expect(resultAction({ success: true })).toEqual({
        type,
        subjectAction: action,
        handlerOpts: {},
        success: true,
        ...defaultOptions,
      });
      expect(success(true)).toEqual({
        type,
        subjectAction: action,
        handlerOpts: {},
        success: true,
        ...defaultOptions,
      });
      expect(resultAction({ error: true })).toEqual({
        type,
        subjectAction: action,
        handlerOpts: {},
        error: true,
        ...defaultOptions,
      });
      expect(error()).toEqual(expect.any(Function));
      expect(clear()).toEqual({
        type: CLEAR_RESULT,
        resultKey: type,
        id: 'latest',
      });
      error()('ERROR').subscribe(_action => {
        expect(_action).toEqual({
          type,
          subjectAction: action,
          handlerOpts: { rawErr: 'ERROR' },
          error: true,
          ...defaultOptions,
        });
      });
    });
  });
});
