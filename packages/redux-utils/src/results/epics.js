import { concat } from 'rxjs';
import { filter, mergeMap, share } from 'rxjs/operators';
import { combineEpics } from 'redux-observable';

import { typeSuffix } from './results-states';
import { devOnlyConsoleHandler } from './handlers';

function filterResults(resultType, result) {
  return (
    result.type.endsWith(typeSuffix) &&
    result[resultType] != null &&
    !result.suppress // suppression of whole result system
  );
}

export function successAggregatorEpic(action$) {
  const resultSuccess$ = action$.pipe(
    filter(
      result =>
        filterResults('success', result) && result.successHandler != null // suppression of aggregation handler
    ),
    share()
  );
  return resultSuccess$.pipe(
    mergeMap(action => action.successHandler(action, resultSuccess$))
  );
}

export function errorAggregatorEpic(action$) {
  const resultErr$ = action$.pipe(
    filter(result => filterResults('error', result)),
    share()
  );
  return resultErr$.pipe(
    mergeMap(action => {
      if (action.errorHandler != null) {
        return concat(
          devOnlyConsoleHandler(action, resultErr$),
          action.errorHandler(action, resultErr$)
        );
      }
      return devOnlyConsoleHandler(action, resultErr$);
    })
  );
}

export default combineEpics(successAggregatorEpic, errorAggregatorEpic);
