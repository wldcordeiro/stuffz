import { of } from 'rxjs';
import { clearResult } from './actions';
import { typeSuffix } from './results-states';

export const defaultOptions = {
  // Result aggregation options
  suppress: false, // Suppress result aggregation & status tracking
  // no default handler for error, opt-in.
  errorHandler: null,
  // no default handler for success, opt-in.
  successHandler: null,
};

/*
 * createResult - given an action type and an optional set of actions returns an
 * API object for creating and accessing results.
 *
 * Return API object properties
 * type - the action type for the result.
 * resultAction - The created action creator function, it is the primitive
 * if you wish to make your own customized forms.
 * success - a simple wrapper around `resultAction` that takes a success argument
 * and handlerOpts argument to customize both
 * error - same as success but for error results.
 * resultSelector - a utility selector for getting the last result of your
 * action's success or failure status.
 *
 * Examples
 *
 * bare
 *
 * const {
 * 	resultAction,
 * 	success,
 * 	error,
 *	clear,
 * 	type,
 * 	resultSelector,
 * } = createResult('MY_ACTION')
 *
 * suppress whole system, just use payloads for your own purposes...
 *
 * const {
 * 	resultAction,
 * 	success,
 * 	error,
 *	clear,
 * 	type,
 * 	resultSelector,
 * } = createResult('MY_ACTION', { suppress: true })
 *
 * use a success handler
 *
 * const {
 * 	resultAction,
 * 	success,
 * 	error,
 *	clear,
 * 	type,
 * 	resultSelector,
 * } = createResult('MY_ACTION', { successHandler: someHandlerFunc })
 *
 * use a different error handler than default
 *
 * const {
 * 	resultAction,
 * 	success,
 * 	error,
 *	clear,
 * 	type,
 * 	resultSelector,
 * } = createResult('MY_ACTION', { errorHandler: someHandlerFunc })
 *
 */
export function createResult(subjectAction, options = defaultOptions) {
  const type = `${subjectAction}${typeSuffix}`;

  function resultAction({ success, error, handlerOpts = {} }) {
    return {
      type,
      subjectAction,
      handlerOpts,
      ...(success != null && { success }),
      ...(error != null && { error }),
      ...{ ...defaultOptions, ...options },
    };
  }

  function success(success = true, handlerOpts) {
    return resultAction({ success, handlerOpts });
  }

  function error(error = true, handlerOpts) {
    return function(err) {
      return of(
        resultAction({
          error,
          handlerOpts: { ...handlerOpts, rawErr: err },
        })
      );
    };
  }

  resultAction.toString = () => type;
  success.toString = () => type;
  error.toString = () => type;

  return {
    type,
    resultAction,
    success,
    error,
    clear(id = 'latest') {
      return clearResult(type, id);
    },
    resultSelector(state, id = 'latest') {
      if (state[type] == null) {
        return state[type];
      }
      return state[type][id];
    },
  };
}
