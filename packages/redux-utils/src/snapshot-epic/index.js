/* eslint-env jest */
import { toArray } from 'rxjs/operators';

export function formatAction(action, serialize, title = 'ACTION') {
  const { type, ...restOfAction } = action;
  const firstLine = `💥 ${title}:\n\ntype: ${type}`;
  const actionHasKeys =
    Object.keys(restOfAction).filter(a => a !== 'type').length > 0;
  if (actionHasKeys) {
    const formattedRest = Object.entries(restOfAction)
      .map(([key, value]) => `${key}: ${serialize(value)}`)
      .join('\n');
    return `${firstLine}\n${formattedRest}\n`;
  }
  return `${firstLine}\n`;
}

export function testForActions(value) {
  return Array.isArray(value) && value.every(i => i.type != null);
}

export function testEpicBody(epic, done) {
  epic.pipe(toArray()).subscribe(actions => {
    expect(actions).toMatchSnapshot();
    !!done && done();
  });
}

export function snapshotEpic(epic, done) {
  expect.addSnapshotSerializer({
    test: testForActions,
    print(actions, serialize) {
      return actions.map(action => formatAction(action, serialize)).join('\n');
    },
  });
  testEpicBody(epic, done);
}
