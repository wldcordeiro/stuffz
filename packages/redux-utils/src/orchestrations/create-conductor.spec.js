/* eslint-env jest */
import { of } from 'rxjs';
import createConductorEpic from './create-conductor';
import { snapshotOrchestration } from './test-orchestration';

const TEST_ACTION = 'TEST_ACTION';
const testAction = jest.fn(() => ({ type: TEST_ACTION }));
const TEST_ACTION2 = 'TEST_ACTION2';
const testAction2 = jest.fn(() => ({ type: TEST_ACTION2 }));
const TEST_ACTION3 = 'TEST_ACTION3';
const testAction3 = jest.fn(() => ({ type: TEST_ACTION3 }));

export const testState = {
  initiated: false,
  thing1: false,
  thing2: false,
};

const testActionMap = {
  [TEST_ACTION]: jest.fn(() => ({
    state: { ...testState, initiated: true },
    nextAction: testAction2(),
  })),
  [TEST_ACTION2]: jest.fn(lastState => ({
    state: { ...lastState, thing1: true },
    nextAction: testAction3(),
  })),
  [TEST_ACTION3]: jest.fn(lastState => ({
    state: { ...lastState, thing2: true },
  })),
};

const testConducterEpic = createConductorEpic(
  TEST_ACTION,
  testActionMap,
  testState
);
const action$ = of(testAction(), testAction2(), testAction3());
const state$ = of({});
const outOfSeq$ = of(testAction2(), testAction3());

describe('createConductorEpic', () => {
  test('normal conductor epic', () => {
    snapshotOrchestration(testConducterEpic(action$, state$));
  });
  test('ignore out of sequence actions when not initiated', () => {
    snapshotOrchestration(testConducterEpic(outOfSeq$, state$));
  });
});
