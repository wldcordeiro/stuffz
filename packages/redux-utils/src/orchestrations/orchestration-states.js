import { ORCHESTRATION_CLEAR, ORCHESTRATION_PROGRESS } from './actions';

export default function orchestrationStates(state = {}, action) {
  switch (action.type) {
    case ORCHESTRATION_PROGRESS:
      return {
        ...state,
        [action.initAction]: {
          ...state[action.initAction],
          ...action.state,
        },
      };
    case ORCHESTRATION_CLEAR:
      return { ...state, [action.id]: undefined };
    default:
      return state;
  }
}
