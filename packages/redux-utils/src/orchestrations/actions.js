export const ORCHESTRATION_CLEAR = 'orchestration/CLEAR';
export function clearOrchestration(id) {
  return { type: ORCHESTRATION_CLEAR, id };
}
export const ORCHESTRATION_PROGRESS = 'orchestration/PROGRESS';
export function orchestrationProgress(initAction, state) {
  return { type: ORCHESTRATION_PROGRESS, initAction, state };
}
