import { concat, empty, of } from 'rxjs';
import { mergeMap, scan, withLatestFrom } from 'rxjs/operators';
import { ofType } from 'redux-observable';
import stringify from 'json-stable-stringify';
import { orchestrationProgress } from './actions';

/*
 * createConductor - this function produces an epic for the purpose of orchestrating
 * sequences of epics. It takes as arguments an `initAction`, an `actionMap`,
 * and an optional `initialStateObject` and produces
 * an epic that will go through your actionMap and call each handler for its
 * action in the sequence, and emit a `progressAction` and `nextAction`.
 *
 * Arguments:
 * initAction - the intializing action
 * actionMap - the actionMap is object map keyed by action types where each member
 * is a handler function called with the following signature.
 *
 * (lastState, action, state, lastPayload) => { state, nextAction, payload }
 *
 * The lastState argument is the state of the sequence (the shape of which is
 * provided by the initStateObj argument documented below.), action is the action
 * the handler is tied to, and store is the redux store, finally `lastPayload`
 * is an optional value to persist contents across steps.
 *
 * Return object format:
 *
 * `state` - the new sequence state
 * `nextAction` - (optional) can be a single action, an array of actions.
 * In the case of a single or many actions those are fired after the progress
 * action while if it is null or undefined we do not fire a next action
 * `payload` - (optional) a user-defined value that can move across steps in the
 * sequence.
 *
 * 	Example actionMap
 *
 *	const TEST_ACTION = 'TEST_ACTION'
 *	const testAction = jest.fn(() => ({ type: TEST_ACTION }))
 *	const TEST_ACTION2 = 'TEST_ACTION2'
 *	const testActionMap = {
 *		[TEST_ACTION]: () => ({
 *			state: { ...testState, initiated: true },
 *			nextAction: testAction2(),
 *		}),
 *		[TEST_ACTION2]: lastState => ({
 *			state: { ...lastState, thing1: true },
 *		}),
 *	}
 *
 * initStateObject - an object of boolean flags for completion of steps in the
 * sequence. The only constraint is that there must be an `initiated` key, the
 * others can be named whatever you chose.
 *
 *	Example initStateObj
 *
 *	const testState = { initiated: false, thing1: false, thing2: false }
 *
 * Return value: conductorEpic - this is your conductor. You must now connect it
 * to the store by passing it to combineEpics
 *
 *	const testConducterEpic = createConductorEpic(
 *		TEST_ACTION,
 *		testActionMap,
 *		testState,
 *	)
 *
 */
export default function createConductorEpic(
  initAction,
  actionMap,
  initStateObj = { initiated: false }
) {
  const scanSeed = {
    state: { ...initStateObj },
    payload: {},
    nextAction: null,
    progressAction: null,
  };
  return function conductorEpic(action$, state$) {
    function mapToTick(lastTick, action, state) {
      const nextTick = actionMap[action.type](
        lastTick.state,
        action,
        state,
        lastTick.payload
      );
      // First check if the sequence was manually failed.
      // Check if our orchestratration has begun, ended or if the step is
      // repeating and don't produce a new progress tick or next action
      const isNotInit = action.type !== initAction;
      const notInitiated =
        !lastTick.state.initiated && !nextTick.state.initiated;
      const seqCompleted = Object.values(lastTick.state).every(v => v === true);
      const seqUnchanged =
        stringify(nextTick.state) === stringify(lastTick.state);
      if (isNotInit && (notInitiated || seqCompleted || seqUnchanged)) {
        return {
          state: lastTick.state,
          payload: lastTick.payload,
          progressAction: null,
          nextAction: null,
        };
      }
      return {
        state: nextTick.state,
        payload: nextTick.payload || lastTick.payload,
        progressAction: orchestrationProgress(initAction, nextTick.state),
        nextAction: [].concat(nextTick.nextAction).filter(i => i),
      };
    }

    return action$.pipe(
      ofType(...Object.keys(actionMap)),
      withLatestFrom(state$),
      scan(
        (lastTick, [action, state]) => mapToTick(lastTick, action, state),
        scanSeed
      ),
      mergeMap(({ progressAction, nextAction }) => {
        if (progressAction == null) {
          return empty();
        }
        const progress$ = of(progressAction);
        if (nextAction == null || nextAction.length === 0) {
          return progress$;
        }
        const nextAction$ = nextAction.map(action => of(action));
        return concat(progress$, ...nextAction$);
      })
    );
  };
}
