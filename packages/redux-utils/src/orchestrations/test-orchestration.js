/* eslint-env jest */
import { formatAction, testForActions, testEpicBody } from '../snapshot-epic';
import { ORCHESTRATION_PROGRESS } from './actions';

function formatProgress(action) {
  const formattedState = Object.entries(action.state)
    .map(([stepName, value]) => `${value ? '✅' : '🔘'}: ${stepName}`)
    .join('\n');
  const progressIcon = Object.values(action.state).every(v => v === true)
    ? '⌛'
    : '⏳';
  return `${progressIcon} ORCHESTRATION PROGRESS:\n\n${formattedState}\n`;
}

const startSep = ' 🚥'.repeat(5);
const start = `${startSep} START${startSep}`;
const endSep = ' 🏁'.repeat(5);
const end = `${endSep} END${endSep}`;
function wrapInSeparator(output) {
  if (output !== '') {
    return `${start}\n\n${output}\n${end}`;
  }
  return output;
}

export function snapshotOrchestration(epic, done) {
  expect.addSnapshotSerializer({
    test: testForActions,
    print(actions, serialize) {
      return wrapInSeparator(
        actions
          .map(action => {
            switch (action.type) {
              case ORCHESTRATION_PROGRESS:
                return formatProgress(action);
              default:
                return formatAction(action, serialize, 'ORCHESTRATION ACTION');
            }
          })
          .join('\n')
      );
    },
  });
  testEpicBody(epic, done);
}
