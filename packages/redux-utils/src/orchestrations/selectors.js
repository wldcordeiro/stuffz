export function getOrchestrationState(state, id) {
  if (state[id] == null) {
    return { initiated: false };
  }
  return state[id];
}

export function getOrchestrationInitiated(state, id) {
  return getOrchestrationState(state, id).initiated;
}
