/* eslint-env jest */
import orchestrationStates from './orchestration-states';
import { orchestrationProgress, clearOrchestration } from './actions';
import { getOrchestrationState, getOrchestrationInitiated } from './selectors';

describe('orchestrationStates reducer', () => {
  const testState = {
    initiated: false,
    thing1: false,
    thing2: false,
    thing3: false,
  };
  const TEST_ACTION = 'TEST_ACTION';
  test('default', () => {
    const state = orchestrationStates(undefined, { type: 'TEST' });
    expect(state).toEqual({});
    expect(getOrchestrationState(state, TEST_ACTION)).toEqual({
      initiated: false,
    });
    expect(getOrchestrationInitiated(state, TEST_ACTION)).toEqual(false);
  });

  test('progress tick', () => {
    let state = orchestrationStates(undefined, { type: 'TEST' });
    expect(state).toEqual({});
    expect(getOrchestrationState(state, TEST_ACTION)).toEqual({
      initiated: false,
    });
    expect(getOrchestrationInitiated(state, TEST_ACTION)).toEqual(false);
    const newProgress = { ...testState, initiated: true };
    state = orchestrationStates(
      state,
      orchestrationProgress(TEST_ACTION, newProgress)
    );
    expect(state).toEqual({ [TEST_ACTION]: newProgress });
    expect(getOrchestrationState(state, TEST_ACTION)).toEqual(newProgress);
    expect(getOrchestrationInitiated(state, TEST_ACTION)).toEqual(true);
  });

  test('clear', () => {
    let state = orchestrationStates(undefined, { type: 'TEST' });
    const newProgress = { ...testState, initiated: true };
    state = orchestrationStates(
      state,
      orchestrationProgress(TEST_ACTION, newProgress)
    );
    expect(state).toEqual({ [TEST_ACTION]: newProgress });
    expect(getOrchestrationState(state, TEST_ACTION)).toEqual(newProgress);
    expect(getOrchestrationInitiated(state, TEST_ACTION)).toEqual(true);
    state = orchestrationStates(state, clearOrchestration(TEST_ACTION));
    expect(state).toEqual({});
    expect(getOrchestrationState(state, TEST_ACTION)).toEqual({
      initiated: false,
    });
    expect(getOrchestrationInitiated(state, TEST_ACTION)).toEqual(false);
  });
});
