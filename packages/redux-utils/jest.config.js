module.exports = {
  displayName: 'redux-utils tests',
  transform: {
    '^.+\\.jsx?$': './babel-transform',
  },
};
