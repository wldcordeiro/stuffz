# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [5.0.0](https://gitlab.com/wldcordeiro/stuffz/compare/v4.0.1...v5.0.0) (2019-01-16)


### Features

* **redux-utils:** change the makeX helpers to be createX ([c068db1](https://gitlab.com/wldcordeiro/stuffz/commit/c068db1))


### BREAKING CHANGES

* **redux-utils:** The names of the functions have changed.





<a name="4.0.1"></a>
## [4.0.1](https://gitlab.com/wldcordeiro/stuffz/compare/v4.0.0...v4.0.1) (2018-09-27)


### Bug Fixes

* **redux-utils:** dep -> peerDep rxjs, redux, redux-observable ([96819b7](https://gitlab.com/wldcordeiro/stuffz/commit/96819b7))





<a name="4.0.0"></a>
# [4.0.0](https://gitlab.com/wldcordeiro/stuffz/compare/v3.1.0...v4.0.0) (2018-09-06)


### Bug Fixes

* **redux-utils:** change API to use `lib/` imports ([d2632b4](https://gitlab.com/wldcordeiro/stuffz/commit/d2632b4)), closes [#2](https://gitlab.com/wldcordeiro/stuffz/issues/2)


### BREAKING CHANGES

* **redux-utils:** No more index exports and change from `dist/` to `lib/` in import paths.





<a name="3.1.0"></a>
# [3.1.0](https://gitlab.com/wldcordeiro/stuffz/compare/v3.0.3...v3.1.0) (2018-09-06)


### Features

* **redux-utils:** add main entry that exports core utils ([f690bef](https://gitlab.com/wldcordeiro/stuffz/commit/f690bef))





<a name="3.0.3"></a>
## [3.0.3](https://gitlab.com/wldcordeiro/stuffz/compare/v3.0.2...v3.0.3) (2018-09-05)


### Bug Fixes

* **redux-utils:** try again to fix publish ([f4a2178](https://gitlab.com/wldcordeiro/stuffz/commit/f4a2178))





<a name="3.0.2"></a>
## [3.0.2](https://gitlab.com/wldcordeiro/stuffz/compare/v3.0.1...v3.0.2) (2018-09-05)


### Bug Fixes

* **redux-utils:** fix `files` array for real ([145583c](https://gitlab.com/wldcordeiro/stuffz/commit/145583c))





<a name="3.0.1"></a>
## [3.0.1](https://gitlab.com/wldcordeiro/stuffz/compare/v3.0.0...v3.0.1) (2018-09-05)


### Bug Fixes

* **redux-utils:** fix the `files` array in package.json ([2504cf9](https://gitlab.com/wldcordeiro/stuffz/commit/2504cf9))





<a name="3.0.0"></a>
# [3.0.0](https://gitlab.com/wldcordeiro/stuffz/compare/v2.1.0...v3.0.0) (2018-09-04)


### Bug Fixes

* **redux-utils:** correct API exports ([d8afd13](https://gitlab.com/wldcordeiro/stuffz/commit/d8afd13))


### BREAKING CHANGES

* **redux-utils:** Any reliance on old exports from the index will need to change like getting the reducer via default.





<a name="2.1.0"></a>
# [2.1.0](https://gitlab.com/wldcordeiro/stuffz/compare/v2.0.1...v2.1.0) (2018-09-04)


### Features

* **redux-utils:** add package for various redux helpers ([f5ba4ef](https://gitlab.com/wldcordeiro/stuffz/commit/f5ba4ef))
