const { resolve } = require('path');
module.exports = require('babel-jest').createTransformer({
  configFile: resolve(__dirname, '../../babel.config.js'),
});
