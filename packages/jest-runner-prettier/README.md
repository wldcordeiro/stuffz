# @wldcordeiro-stuffz/jest-runner-prettier

## Usage

### Install

Install `jest`_(it needs Jest 21+)_ and `@wldcordeiro-stuffz/jest-runner-prettier`

```bash
yarn add --dev jest @wldcordeiro-stuffz/jest-runner-prettier

# or with NPM

npm install --save-dev jest @wldcordeiro-stuffz/jest-runner-prettier
```

### Add it to your Jest config

In your `package.json`

```json
{
  "jest": {
    "runner": "@wldcordeiro-stuffz/jest-runner-prettier",
    "moduleFileExtensions": [
      "js",
      "jsx",
      "json",
      "ts",
      "tsx",
      "css",
      "less",
      "scss",
      "graphql",
      "md",
      "markdown"
    ],
    "testMatch": [
      "**/*.js",
      "**/*.jsx",
      "**/*.json",
      "**/*.ts",
      "**/*.tsx",
      "**/*.css",
      "**/*.less",
      "**/*.scss",
      "**/*.graphql",
      "**/*.md",
      "**/*.markdown"
    ]
  }
}
```

Or in `jest.config.js`

```js
module.exports = {
  runner: '@wldcordeiro-stuffz/jest-runner-prettier',
  moduleFileExtensions: [
    'js',
    'jsx',
    'json',
    'ts',
    'tsx',
    'css',
    'less',
    'scss',
    'graphql',
    'md',
    'markdown'
  ],
  testMatch: [
    '**/*.js',
    '**/*.jsx',
    '**/*.json',
    '**/*.ts',
    '**/*.tsx',
    '**/*.css',
    '**/*.less',
    '**/*.scss',
    '**/*.graphql',
    '**/*.md',
    '**/*.markdown'
  ]
}
```

### Run Jest

```bash
yarn jest
```
