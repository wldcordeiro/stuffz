# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [5.0.0](https://gitlab.com/wldcordeiro/stuffz/compare/v4.0.1...v5.0.0) (2019-01-16)

**Note:** Version bump only for package @wldcordeiro-stuffz/jest-runner-prettier





<a name="4.0.1"></a>
## [4.0.1](https://gitlab.com/wldcordeiro/stuffz/compare/v4.0.0...v4.0.1) (2018-09-27)


### Bug Fixes

* **redux-utils:** dep -> peerDep rxjs, redux, redux-observable ([96819b7](https://gitlab.com/wldcordeiro/stuffz/commit/96819b7))





<a name="4.0.0"></a>
# [4.0.0](https://gitlab.com/wldcordeiro/stuffz/compare/v3.1.0...v4.0.0) (2018-09-06)

**Note:** Version bump only for package @wldcordeiro-stuffz/jest-runner-prettier





<a name="3.0.0"></a>
# [3.0.0](https://gitlab.com/wldcordeiro/stuffz/compare/v2.1.0...v3.0.0) (2018-09-04)


### Bug Fixes

* **redux-utils:** correct API exports ([d8afd13](https://gitlab.com/wldcordeiro/stuffz/commit/d8afd13))


### BREAKING CHANGES

* **redux-utils:** Any reliance on old exports from the index will need to change like getting the reducer via default.





<a name="2.1.0"></a>
# [2.1.0](https://gitlab.com/wldcordeiro/stuffz/compare/v2.0.1...v2.1.0) (2018-09-04)


### Features

* **redux-utils:** add package for various redux helpers ([f5ba4ef](https://gitlab.com/wldcordeiro/stuffz/commit/f5ba4ef))





<a name="2.0.1"></a>
## [2.0.1](https://gitlab.com/wldcordeiro/stuffz/compare/v2.0.0...v2.0.1) (2018-09-04)

**Note:** Version bump only for package @wldcordeiro-stuffz/jest-runner-prettier





<a name="2.0.0"></a>
# 2.0.0 (2018-08-31)

**Note:** Version bump only for package @wldcordeiro-stuffz/jest-runner-prettier
