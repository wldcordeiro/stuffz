# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [5.0.0](https://gitlab.com/wldcordeiro/stuffz/compare/v4.0.1...v5.0.0) (2019-01-16)


### Features

* **redux-utils:** change the makeX helpers to be createX ([c068db1](https://gitlab.com/wldcordeiro/stuffz/commit/c068db1))


### BREAKING CHANGES

* **redux-utils:** The names of the functions have changed.





<a name="4.0.1"></a>
## [4.0.1](https://gitlab.com/wldcordeiro/stuffz/compare/v4.0.0...v4.0.1) (2018-09-27)


### Bug Fixes

* **redux-utils:** dep -> peerDep rxjs, redux, redux-observable ([96819b7](https://gitlab.com/wldcordeiro/stuffz/commit/96819b7))





<a name="4.0.0"></a>
# [4.0.0](https://gitlab.com/wldcordeiro/stuffz/compare/v3.1.0...v4.0.0) (2018-09-06)

**Note:** Version bump only for package @wldcordeiro-stuffz/snapshot-diff-serializer





<a name="3.0.0"></a>
# [3.0.0](https://gitlab.com/wldcordeiro/stuffz/compare/v2.1.0...v3.0.0) (2018-09-04)

**Note:** Version bump only for package @wldcordeiro-stuffz/snapshot-diff-serializer





<a name="2.0.0"></a>
# 2.0.0 (2018-08-31)

**Note:** Version bump only for package @wldcordeiro-stuffz/snapshot-diff-serializer
