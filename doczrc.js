const { resolve } = require('path');

const DOCS_ROOT = resolve(__dirname, 'docs');

const modifyBundlerConfig = config => {
  config.resolve.alias = Object.assign({}, config.resolve.alias, {
    '@components': `${DOCS_ROOT}/components`,
    '@images': `${DOCS_ROOT}/images`,
  });

  return config;
};

module.exports = {
  description: 'My various libraries and tools I use',
  hashRouter: true,
  modifyBundlerConfig,
  title: 'wldcordeiro stuffz',
};
